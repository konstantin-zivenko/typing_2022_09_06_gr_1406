# String thing;
# thing = 1; Error!!!

class TheHobbit:
    def __len__(self):
        return 2

class TheOrc:
    def __len__(self):
        return 100


def headline(text: str, align: bool = True):
    if align:
        return f"{text.title()}\n{'-' * len(text)}"
    else:
        return f" {text.title()} ".center(50, "o")


print(headline("text"))


print(headline("text", align=""))

# Mypy